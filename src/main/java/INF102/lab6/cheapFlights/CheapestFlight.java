package INF102.lab6.cheapFlights;

import java.security.spec.ECFieldF2m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import INF102.lab6.graph.DirectedEdge;
import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph <City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph <City, Integer> g = new WeightedDirectedGraph<>();
        for (Flight flight : flights) {
            City start = flight.start;
            City dest = flight.destination;
            g.addVertex(start);
            g.addVertex(dest);
            Integer cost = flight.cost;
            g.addEdge(start, dest, cost);
        }
        return g;
    }

    class TripInfo {
        Integer stopCount;
        City destination;

        public TripInfo(Integer stopCount, City destination) {
            this.stopCount = stopCount;
            this.destination = destination;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            else if (!(o instanceof TripInfo)) {
                return false;
            }
            TripInfo other = (TripInfo) o;
            return this.stopCount.equals(other.stopCount) && this.destination.equals(other.destination);
        }
    }

    class Trip implements Comparable <Trip> {
        TripInfo tripInfo;
        Integer price;

        public Trip(TripInfo tripInfo, Integer price) {
            this.tripInfo = tripInfo;
            this.price = price;
        }

        public int compareTo(Trip o) {
            return Integer.compare(price, o.price); 
        }
    }

    private void addNeighbours(WeightedDirectedGraph<City, Integer> g, Trip startTrip, PriorityQueue<Trip> tripQ) {
        City currentCity = startTrip.tripInfo.destination;
        for (City city : g.outNeighbours(currentCity)) {
            TripInfo tripInfo = new TripInfo(startTrip.tripInfo.stopCount + 1, city);
            Integer cost = g.getWeight(currentCity, city);
            Trip trip = new Trip(tripInfo, startTrip.price + cost);
            tripQ.add(trip);
        }
    }
    
    public HashMap <City, Integer> djikstra(WeightedDirectedGraph <City, Integer> g, City start, int nMaxStops) {
        HashMap <TripInfo, Trip> tripMap = new HashMap<>();
        PriorityQueue <Trip> tripQ = new PriorityQueue<>();
        TripInfo startInfo = new TripInfo(0, start);
        Trip startTrip = new Trip(startInfo, 0);
        addNeighbours(g, startTrip, tripQ);
        tripMap.put(startInfo, startTrip);

        while (!tripQ.isEmpty()) {
            Trip currTrip = tripQ.remove();
            if (tripMap.containsKey(currTrip.tripInfo)) {
                continue;
            }
            tripMap.put(currTrip.tripInfo, currTrip);
            if (currTrip.tripInfo.stopCount <= nMaxStops) {
                addNeighbours(g, currTrip, tripQ);
            }
        }

        HashMap <City, Integer> prices = new HashMap<>();
        for (Trip trip : tripMap.values()) {
            City city = trip.tripInfo.destination;
            Integer stops = trip.tripInfo.stopCount;
            Integer cost = trip.price;
            if (stops > nMaxStops + 1) {
                continue;
            }
            if (!prices.containsKey(city) || prices.get(city) > cost) {
                prices.put(city, cost);
            }
        }
        return prices;
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph <City, Integer> g = constructGraph(flights);
        HashMap <City, Integer> prices = djikstra(g, start, nMaxStops);
        return prices.get(destination);
    }
}